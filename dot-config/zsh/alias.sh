## Alias section
alias cp="cp -i"                                                # Confirm before overwriting something
alias mv="mv -i"                                                # Confirm before overwriting something
alias df='df -h'                                                # Human-readable sizes
alias free='free -m'                                            # Show sizes in MB
alias ls='ls --color=auto'                                      # ls with colors
alias grep='grep --color=auto'                                  # grep with colors
alias diff='diff --color=auto'                                  # diff with colors
alias gETH='cd /home/samuel/Documents/Schulisches/Studium/ETH/'
alias config='/usr/bin/git --git-dir=$HOME/.cfg --work-tree=$HOME'
alias sqlite='sqlite3 -init ~/.config/sqlite3/sqliterc'
alias la='ls -al'

export PATH=$PATH:/var/lib/flatpak/exports/share:/home/samuel/.local/share/flatpak/exports/share/applications
