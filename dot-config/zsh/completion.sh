#vim: set filetype=zsh
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # automatically find new executables in path
# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zsh/cache
# sets style of the completion menu: makes it a menu
zstyle ':completion:*' menu select # enables menu to select completion
zstyle ':completion:*:descriptions' format '%B%d%b' # puts a menu title above every category of completion
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d' # display a error message if no matches were found
zstyle ':completion:*' group-name ''
zmodload zsh/complist
