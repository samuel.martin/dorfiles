## Autostart sway upon login when login shell
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec sh ~/Projects/WM-Notes/run_sway.sh
fi

## Load ~/.profile to load the environment variable changes from there
source ~/.profile

# Path to the directory with this and other config files
CONFIG_DIR=$XDG_CONFIG_HOME/zsh

## Options section
setopt correct                                                  # Auto correct mistakes
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expension with parameters
setopt nocheckjobs                                              # Don't warn about running processes when exiting
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt appendhistory                                            # Immediately append history instead of overwriting
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt autocd                                                   # if only directory path is entered, cd there.


HISTFILE="$XDG_DATA_HOME"/zsh/history
HISTSIZE=2000
SAVEHIST=2000
export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
export TERM=xterm-256color
# export MANPAGER=less
export MANPAGER='bat --language man --plain --paging="always"'
# export MANPAGER="sh -c 'col -bx | bat -l man -p'"               # Use bat as man pager
# export MANPAGER="nvim -c 'set ft=man' -u ~/.config/nvim/man-init.vim -"
WORDCHARS=${WORDCHARS//\/[&.;]}                                 # Don't consider certain characters part of the word

source $CONFIG_DIR/completion.sh
source $CONFIG_DIR/keybindings.sh
source $CONFIG_DIR/alias.sh
source $CONFIG_DIR/prompt.sh
source $CONFIG_DIR/plugins.sh
source $CONFIG_DIR/vim_cursor.sh
source $CONFIG_DIR/theming.sh



# Apply different settigns for different terminals
case $(basename "$(cat "/proc/$PPID/comm")") in
  login)
    	RPROMPT="%{$fg[red]%} %(?..[%?])" 
    	alias x='startx ~/.xinitrc'      # Type name of desired desktop after x, xinitrc is configured for it
    ;;
#  'tmux: server')
#        RPROMPT='$(git_prompt_string)'
#		## Base16 Shell color themes.
#		#possible themes: 3024, apathy, ashes, atelierdune, atelierforest, atelierhearth,
#		#atelierseaside, bespin, brewer, chalk, codeschool, colors, default, eighties, 
#		#embers, flat, google, grayscale, greenscreen, harmonic16, isotope, londontube,
#		#marrakesh, mocha, monokai, ocean, paraiso, pop (dark only), railscasts, shapesifter,
#		#solarized, summerfruit, tomorrow, twilight
#		#theme="eighties"
#		#Possible variants: dark and light
#		#shade="dark"
#		#BASE16_SHELL="/usr/share/zsh/scripts/base16-shell/base16-$theme.$shade.sh"
#		#[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL
#		# Use autosuggestion
#		source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
#		ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
#  		ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'
#     ;;
  *)
        # RPROMPT='$(git_prompt_string)'
		# Use autosuggestion
		source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
		ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
  		ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'
    ;;
esac

zle -N zle-line-init
echo -ne $insert_mode_cursor # use beam shape cursor on startup
preexec() { echo -ne $insert_mode_cursor; } # use beam shape cursor for each new prompt


#nvm
export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
