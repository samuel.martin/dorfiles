# change cursor shape for different vi mode
insert_mode_cursor='\e[5 q'
normal_mode_cursor='\e[1 q'
function zle-keymap-select {
    if [[ ${KEYMAP} == vicmd || $1 = 'block' ]]
    then
        echo -ne $normal_mode_cursor
    elif [[ ${KEYMAP} == main || ${KEYMAP} == viins || ${KeyMAP} == '' || $1 = 'beam' ]]
    then
        echo -ne $insert_mode_cursor
    fi
}

zle -N zle-keymap-select
zle-line-init() {
    echo -ne $insert_mode_cursor
}
