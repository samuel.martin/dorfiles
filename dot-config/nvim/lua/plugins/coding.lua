-- LSP pluggins and other plugins helping with writing code

return {
    {
        'williamboman/mason-lspconfig.nvim'
    },
    {
        'neovim/nvim-lspconfig',
    },
    {
        'nvimdev/lspsaga.nvim',
        config = function()
            require('lspsaga').setup({
                symbol_in_winbar = {
                    enable = true
                }
            })
        end,
        dependencies = {
            'nvim-treesitter/nvim-treesitter', -- optional
            'nvim-tree/nvim-web-devicons',     -- optional
        },
    },
    {
        'nvim-lua/lsp-status.nvim',
        opts = {
            current_function = true,
            show_filename = true,
            diagnostics = false,
            kind_labels = {
                Function = '󰊕',
                Method = '',
                Interface = '',
            }
        },
        config = function(_, opts)
            require('lsp-status').config(opts)
            require('lsp-status').register_progress()
        end,
    },
    {
        'hrsh7th/nvim-cmp', 
        dependencies = {
            'hrsh7th/cmp-nvim-lsp',
            'saadparwaiz1/cmp_luasnip',
            {'L3MON4D3/LuaSnip',
                build = "make install_jsregexp",
                -- TODO: Somehow the snippets don't seem to load
                config = function(_, opts)
                    require("luasnip.loaders.from_vscode").lazy_load({paths = {"~/.config/nvim/snippets", "./snippets"}})
                end,
            },
        },
        opts = {
            sources = {
                { name = 'orgmode'}
            }
        },
        config = function()
            cmp = require('cmp')
            luasnip = require('luasnip')

            local kind_icons = {
                Text = '',
                Method = '',
                Function = '󰊕',
                Constructor = '',
                Field = '',
                Variable = '',
                Class = '',
                Interface = '',
                Module = '',
                Property = '',
                Unit = '',
                Value = '',
                Enum = '',
                Keyword = '',
                Snippet = '',
                Color = '',
                File = '',
                Reference = '',
                Folder = '',
                EnumMember = '',
                Constant = '',
                Struct = '',
                Event = '',
                Operator = 'ﬦ',
                TypeParameter = '',
            }

            cmp.setup {
                completion = {
                    autocomplete = false,
                },
                snippet = {
                    expand = function(args)
                        luasnip.lsp_expand(args.body)
                    end,
                },
                mapping = {
                    ['<C-k>'] = cmp.mapping.select_prev_item(),
                    ['<C-j>'] = cmp.mapping.select_next_item(),
                    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
                    ['<C-f>'] = cmp.mapping.scroll_docs(4),
                    ['<C-Space>'] = cmp.mapping.complete(),
                    ['<C-e>'] = cmp.mapping.close(),
                    ['<CR>'] = cmp.mapping.confirm({
                        behavior = cmp.ConfirmBehavior.Replace,
                        select = true,
                    }),
                    ['<Tab>'] = function(fallback)
                        if cmp.visible() then
                            cmp.select_next_item()
                        elseif luasnip.expand_or_jumpable() then
                            luasnip.expand_or_jump()
                        else
                            fallback()
                        end
                    end,
                    ['<S-Tab>'] = function(fallback)
                        if cmp.visible() then
                            cmp.select_prev_item()
                        elseif luasnip.jumpable(-1) then
                            luasnip.jump(-1)
                        else
                            fallback()
                        end
                    end,
                },
                sources = cmp.config.sources {
                    { name = 'nvim_lsp' },
                    { name = 'buffer' },
                },
                formatting = {
                    fields = {'kind', 'abbr', 'menu'},
                    format = function(entry, vim_item)
                        -- Kind icons
                        -- Use icon and text
                        -- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind)
                        -- Use only icon
                        vim_item.kind = kind_icons[vim_item.kind]
                        -- Source
                        vim_item.menu = ({
                            buffer = "[Buffer]",
                            nvim_lsp = "[LSP]",
                            luasnip = "[LuaSnip]",
                            nvim_lua = "[Lua]",
                            latex_symbols = "[LaTex]",
                        })[entry.source.name]
                        return vim_item
                    end,
                },
            }
        end,
    },
    {
        "ray-x/lsp_signature.nvim",
      -- event = "VeryLazy",
        opts = {
            floating_window = false,
            hint_enable = true,
            hint_prefix = {
                above = "↙ ",  -- when the hint is on the line above the current line
                current = "← ",  -- when the hint is on the same line
                below = "↖ "  -- when the hint is on the line below the current line
            },
            hint_inline = function() return eol end,
        },
        config = function(_, opts)
          require'lsp_signature'.setup(opts)
        end
    }
}
-- LSP Plugin
