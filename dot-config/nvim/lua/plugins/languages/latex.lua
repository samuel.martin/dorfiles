return {
    {
      "lervag/vimtex",
      lazy = false, -- lazy-loading will disable inverse search
      config = function()
        vim.api.nvim_create_autocmd({ "FileType" }, {
          group = vim.api.nvim_create_augroup("lazyvim_vimtex_conceal", { clear = true }),
          pattern = { "bib", "tex" },
          callback = function()
            vim.wo.conceallevel = 2
          end,
        })

        vim.g.vimtex_view_general_viewer = 'zathura'
        vim.g.vimtex_compiler_latexmk = {
            callback = 1,
            continuous = 1,
            options = {
                "-shell-escape",
                "-verbose",
                "-file-line-error",
                "-synctex=1",
                "-interaction=nonstopmode",
            },
        }
      end,
    },
}
