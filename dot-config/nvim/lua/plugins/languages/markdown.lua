return {
    {
        "epwalsh/obsidian.nvim",
        version = "*",  -- recommended, use latest release instead of latest commit
        ft = "markdown",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        opts = {
            workspaces = {
                {
                    name = 'notes',
                    path = '~/Documents/NextcloudNotes/'
                },
            },
            follow_url_func = function(url) 
                vim.fn.jobstart({'xdg-open', url})
            end,
        }
    },
    {
        "https://github.com/preservim/vim-markdown"
    }
}
