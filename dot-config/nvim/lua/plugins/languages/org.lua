return {
    {
        'nvim-orgmode/orgmode',
        dependencies = {
            {
                -- Replace axterisks with bullets
                'akinsho/org-bullets.nvim'
            },

            { 'nvim-treesitter/nvim-treesitter', lazy = true },
        },
        event = 'VeryLazy',
        config = function()
            -- Load treesitter grammar for org
            require('orgmode').setup_ts_grammar()

            -- Setup treesitter
            require('nvim-treesitter.configs').setup({
                highlight = {
                    enable = true,
                    additional_vim_regex_highlighting = { 'org' },
                },
                ensure_installed = { 'org' },
            })

            -- Setup orgmode
            require('orgmode').setup({
                org_agenda_files = '~/Documents/Notes/Org/*.org',
                org_default_notes_file = '~/Documents/Notes/Org/main.org',
            })
        end,
    }
}
