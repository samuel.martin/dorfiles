return {
    {
        'folke/neodev.nvim',
        opts = {
            library = {
                plugins = {'nvim-dap-ui', 'neotest'},
                types=true
            },
        }
    },
}
