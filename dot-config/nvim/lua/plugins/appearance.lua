-- Plugins changing the visual appearance

local function indentation()
    return vim.bo.shiftwidth..''
end

local function lspStatus()
    local lsp_status = require('lsp-status')
    local status = lsp_status.status()
    return status
end

local function workingDir()
    local working_dir = vim.fn.getcwd()
    local home = os.getenv('HOME')
    return working_dir:gsub("^" .. home, "~")
    
end

return {
    -- Powerline & Themes
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons'},
        opts = {
            options = {
                icons_enabled = true,
                theme = 'catppuccin',
                section_separators = {'', ''},
                component_separators = {'', ''},
                disabled_filetypes = {
                    winbar = {'neo-tree'}
                },
                globalstatus = true,
            },
            sections = {
                lualine_a = {'mode'},
                lualine_b = {'branch', },
                lualine_c = {{workingDir}, 'filename', {lspStatus}, },
                lualine_x = {
                    { 
                        'diagnostics',
                        sources = {"nvim_diagnostic"},
                        symbols = {error = ' ', warn = ' ', info = ' ', hint = ' '}
                    },
                    {indentation},
                    'encoding',
                    'filetype'
                },
                lualine_y = {'progress'},
                lualine_z = {'location'}
              },
            inactive_sections = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = {'filename'},
                lualine_x = {'location'},
                lualine_y = {},
                lualine_z = {}
            },
            tabline = {},
            extensions = {'fugitive'}
        },
    },
    -- Colorscheme
    {
        'Tsuzat/NeoSolarized.nvim',
        opts = {
            style = "dark",
            transparent = true,
            enable_italics = false,
            styles = {
                comments = {italic = false},
                keywords = {italic = false},
                string = {italic=false},
            }
        }
    },
    {
        "catppuccin/nvim",
        config = function(_, opts)
            require('catppuccin').setup(opts)
        end,
        opts = {
            background = {
                light = "latte",
                dark = "mocha",
            },
            transparent_background = false,
            dim_inactive = {
                enabled = true,
                shade = 'dark',
                percentage = 0.90,
            },
            styles = {
                comments = {},
                conditionals = {},
            },
            integrations = {
                dap = true,
                dap_ui = true,
                cmp = true,
                gitgutter = true,
                neotree = true,
                treesitter = true,
                lsp_saga = true,
                markdown = true,
                mason = true,
                illuminate = {
                    enabled = true,
                    lsp = true,
                },
                which_key = true,
                native_lsp = {
                    enabled = true,
                    virtual_text = {
                        errors = { "italic" },
                        hints = { "italic" },
                        warnings = { "italic" },
                        information = { "italic" },
                    },
                    underlines = {
                        errors = { "underline" },
                        hints = { "underline" },
                        warnings = { "underline" },
                        information = { "underline" },
                    },
                    inlay_hints = {
                        background = true,
                    },
                },
            }
        }
    },
    -- Icons
    {
        'ryanoasis/vim-devicons'

    },
    -- highlight other occurances of current word
    {
        'RRethy/vim-illuminate',
        opts = {
            delay = 800,
            large_file_cutoff = 2000,
            large_file_overrides = {
                providers = { "lsp" },
            },
            filetypes_denylist = {
                'fugitive', 'help'
            },
        },
        config = function(_, opts)
            require('illuminate').configure(opts)
        end
    },
    {
        'norcalli/nvim-colorizer.lua',
        opts = {
            'css',
            'javascript',
        }
    },
    {
        'nanozuki/tabby.nvim',
        dependencies = 'nvim-tree/nvim-web-devicons',
        config = function()
            require('tabby').setup({
                preset = 'tab_only',
            })
        end,
    },
    {
      "lukas-reineke/indent-blankline.nvim",
      main = "ibl",
      ---@module "ibl"
      ---@type ibl.config
      opts = {},
      config = function(_, opts)
        require("ibl").setup(opts)
      end
    }
}

