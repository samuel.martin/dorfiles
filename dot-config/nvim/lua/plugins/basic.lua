-- Plugins which provide basic functionality, often used by other plugins

return {
    {
        'nvim-treesitter/nvim-treesitter',
        build = function()
            require("nvim-treesitter.install").update({ with_sync = true })()
        end,
        config = function()
            require('nvim-treesitter.configs').setup({
                ensure_installed = {"python", "cpp", "rust", "latex", "lua", "org", "dart"},
                sync_install = false,
                auto_install = false,
                highlight = {
                    enable = true,
                -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
                -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
                -- Using this option may slow down your editor, and you may see some duplicate highlights.
                -- Instead of true it can also be a list of languages
                additional_vim_regex_highlighting = {'org'},
                indent = {enable = true},
                },
            })
        end,
    },
    {
        'https://github.com/nvim-treesitter/nvim-treesitter-context',
        config = function()
            require('treesitter-context').setup({
                max_lines = 0
            })
        end
    },
    {
        'nvim-telescope/telescope.nvim', tag = '0.1.8',
        dependencies = {
            'nvim-lua/plenary.nvim',
            --'nvim-lua/popup.nvim'
            },
        opts = {
            defaults = {
                mappings = {
                    i = {
                        ["<C-j>"] = "move_selection_next",
                        ["<C-k>"] = "move_selection_previous",
                        ["<esc>"] = "close"
                     }
                },
                path_display = {shorten = {len = 3}},
            }
        }
    },
    -- Mason to install LSP, linters... and other programs
    {
        'williamboman/mason.nvim',
        config = function()
            require('mason').setup()
        end
    },
    {
        'RubixDev/mason-update-all'
    },
}

