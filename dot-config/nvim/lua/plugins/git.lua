return {
    --Git
    --Git symbols in the sign column
    {
        'airblade/vim-gitgutter',
        config = function(_, opts)
            -- Disable default keymappings
            vim.g.gitgutter_map_keys=0
        end,
    },
    -- Git integration
    {
        'tpope/vim-fugitive'
    },
    {
        'sindrets/diffview.nvim',
        config = function()
            require('diffview').setup({
                default = {
                    winbar_info = true,
                    layout = "diff2_vertical",
                },
                merge_tool = {
                    layout = 'diff3_mixed',
                },
                keymaps = {
                }
            })
        end
    },

}
