return {
    {
        "nvim-neotest/neotest",
        requires = {
            "nvim-neotest/nvim-nio",
            "nvim-lua/plenary.nvim",
            "antoinemadec/FixCursorHold.nvim",
            "nvim-treesitter/nvim-treesitter",
            "sidlatau/neotest-dart",
        },
        opts = {
            adapters = {
                {
                    command = 'flutter',
                    use_lsp = true,
                }
            }
        }
    }
}
