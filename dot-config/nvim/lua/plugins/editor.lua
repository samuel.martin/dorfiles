-- Plugins adding editor such as file tree window, commenting etc.

return {
    -- File tree window
    {
        "nvim-neo-tree/neo-tree.nvim",
        branch = "v3.x",
        dependencies = {
          "nvim-lua/plenary.nvim",
          "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
          "MunifTanjim/nui.nvim",
          -- "3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
        },
        opts = {
            event_handlers = {
                {
                    event = 'file_opened',
                    handler = function(file_path)
                        require('neo-tree.command').execute({action = 'close'})
                    end
                }
            },
            filesystem = {
                filtered_items = {
                    visible = true,
                    hide_hidden = false
                }
            }
        }
    },
    -- Code outline
    {
        'hedyhli/outline.nvim',
        config = function()
            require('outline').setup({})
        end
    },
    -- Easy control and description of leader key commands (popup when leader key is presed)
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
    },
    -- Provides (un)commenting commands
    {
        'tpope/vim-commentary'
    },
    -- Code Documentation
    {
        "danymat/neogen", 
        config = function()
            require('neogen').setup()
        end,
        version = "*" 
    },
    {
        'sbdchd/neoformat',
        config = function()
            local g = vim.g
            g.neoformat_enabled_python = {'yapf'}
        end,
    },
    {
    "folke/trouble.nvim",
      opts = {}, -- for default options, refer to the configuration section for custom setup.
      cmd = "Trouble",
  },
  {
      "folke/persistence.nvim",
      event = "BufReadPre", -- this will only start session saving when an actual file was opened
      opts = {
          -- add any custom options here
      }
  },
  {
    "ahmedkhalf/project.nvim",
    opts = {
    },
    config = function(_, opts)
      require("project_nvim").setup(opts)
      require("telescope").load_extension("projects")
    end,
  }
}
