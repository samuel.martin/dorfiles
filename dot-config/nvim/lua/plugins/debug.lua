return {
    {
        'mfussenegger/nvim-dap',
        dependencies = 
        {
            {
                'rcarriga/nvim-dap-ui',
                opts = {
                    layouts = {
                        {
                            elements = {
                                {
                                    id = "breakpoints",
                                    size = 0.1
                                },
                                {
                                    id = "stacks",
                                    size = 0.1
                                },
                                {
                                    id = "scopes",
                                    size = 0.4
                                },
                                {
                                    id = "watches",
                                    size = 0.4
                                },
                            },
                            position = "right",
                            size = 100
                        },
                        {
                            elements = {
                                {
                                    id = "repl",
                                    size = 0.6
                                },
                                {
                                    id = "console",
                                    size = 0.4
                                }
                            },
                            position = "bottom",
                            size = 10,
                        }
                    }
                },
            },
            {
                'theHamsta/nvim-dap-virtual-text',
                config = function()
                    require('nvim-dap-virtual-text').setup({})
                end
            },
            {
                'mfussenegger/nvim-dap-python',
                config = function()
                    require('dap-python').setup("python")
                end
            },
            {   'nvim-neotest/nvim-nio'}
        },
        config = function(_, opts)
            local dap = require('dap')
            dap.adapters.lldb = {
                type = 'executable',
                command = '/usr/bin/lldb-vscode',
                name = 'lldb'
            }
            dap.adapters.dart = {
              type = 'executable',
              command = 'dart',
              args = {'debug_adapter'}
            }
            dap.adapters.flutter = {
              type = 'executable',
              command = 'flutter',
              args = {'debug_adapter'}
          }
            dap.adapters.gdb = {
                type = "executable",
                command = "gdb",
                args = { "-i", "dap" }
            }
            dap.adapters.cppdbg = {
                id = "cppdbg",
                type = "executable",
                command = "/home/samuel/.local/share/nvim/mason/bin/OpenDebugAD7"
            }
            dap.configurations.fortran = {
                {
                    name = "Launch",
                    type = "lldb",
                    request = "launch",
                    program = "${workspaceFolder}/build/bin/dwarf-cloudsc-fortran",
                    cwd = '${workspaceFolder}/build',
                    stopOnEntry = false,
                    args = {"1", "1", "1"},
                }
            }
            dap.configurations.dart = {
                {
                    type = 'dart',
                    request = 'launch',
                    name = 'Launch this file',
                    dartSdkPath = '/home/samuel/snap/flutter/common/flutter/bin/cache/dart-sdk/bin/dart',
                    flutterSdkPath = '/home/samuel/snap/flutter/common/flutter/bin/flutter',
                    program = '${file}',
                    cwd = '${workspaceFolder}',
                },
                {
                    type = 'dart',
                    request = 'launch',
                    name = 'Launch flutter app',
                    dartSdkPath = '/home/samuel/snap/flutter/common/flutter/bin/cache/dart-sdk/bin/dart',
                    flutterSdkPath = '/home/samuel/snap/flutter/common/flutter/bin/flutter',
                    program = '${workspaceFolder}/lib/main.dart',
                    cwd = '${workspaceFolder}',
                }
            }
        end,
    },
}
