return {
    {
        'mfussenegger/nvim-lint',
        config = function()
            lint = require('lint')
            lint.linters.clangtidy.args = {
                '-p', 'build'
            }
            lint.linters_by_ft = {
                c = {'cppcheck', 'clangtidy'},
                tex = {'proselint',},
                markdown = {'proselint',},
                python = {'flake8', }
            }
        end,
    },
}
