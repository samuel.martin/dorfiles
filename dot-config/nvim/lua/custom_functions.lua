--- Places a given character at a given column position
-- @param char The character to place
-- @param column The column to place it at
function placeCharAtColumn(char, column)
    -- Get position of cursor
    local row, col = unpack(vim.api.nvim_win_get_cursor(0))
    -- Get content of the current line
    local line = vim.api.nvim_get_current_line()

    -- calculate needed spaces & cap
    local space_count = column - #line - 1
    if space_count < 0 then
        space_count = 0
    end
    
    local str_to_insert = string.rep(" ", space_count) .. char

    vim.api.nvim_set_current_line(line .. str_to_insert)
end

function makeCommentBlock(char, indentation)
end

--- Creates a line of given characters until a given column
-- Will append it to the existing line content
-- @param char The character used to create the line
-- @param untilColumn The column until which the line should go
function createLine(char, untilColumn)
    -- Get position of cursor
    local row, col = unpack(vim.api.nvim_win_get_cursor(0))
    -- Get content of the current line
    local line = vim.api.nvim_get_current_line()
    -- calculate how many columns there are until the desired one
    local lineLength = untilColumn - #line

    if lineLength > 0 then
        local lineToAdd = string.rep(char, lineLength)
        vim.api.nvim_set_current_line(line .. lineToAdd)
    end
end






