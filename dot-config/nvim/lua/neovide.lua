vim.o.guifont = "Hack Nerd Font Mono:h11"
vim.g.neovide_transparency = 0.9
-- Should follow system, doesn't seem to work on my system
vim.g.neovide_theme = 'auto'
vim.g.neovide_floating_blur_amount_x = 2.0
vim.g.neovide_floating_blur_amount_y = 2.0

vim.g.neovide_scale_factor = 1.0
vim.g.neovide_fullscreen = true

local change_scale_factor = function(delta)
  vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta
end
vim.keymap.set("n", "<C-=>", function()
  change_scale_factor(1.25)
end)
vim.keymap.set("n", "<C-->", function()
  change_scale_factor(1/1.25)
end)
