local lsp = require('lspconfig')
local cmp = require('cmp')
local luasnip = require('luasnip')
local lsp_status = require('lsp-status')

-- Add additional capabilities supported by nvim-cmp
local capabilities = require('cmp_nvim_lsp').default_capabilities()

-- set completeopt to have better comletion experience
vim.o.completeopt = 'menuone,noselect'

lsp.pylsp.setup {
    on_attach = lsp_status.on_attach,
    capabilities = lsp_status.capabilities,
    settings = {
        pylsp = {
            plugins = {
                autopep8 = {
                    enabled = false
                },
                flake8 = {
                    enabled = true
                },
                pylint = {
                    enabled = false
                },
                pycodestyle = {
                    enabled = false
                },
                mccabe = {
                    enabled = false
                },
                pyflakes = {
                    enabled = false
                }
            }
        }
    }
}

lsp.ruff.setup({
    on_attach = lsp_status.on_attach,
    capabilities = lsp_status.capabilities,
    trace = 'messages',
    init_options = {
        settings = {
            logLevel = 'debug'
        }
    }
})

lsp.clangd.setup{
    on_attach = lsp_status.on_attach,
    capabilities = lsp_status.capabilities,
    filetypes = {"c", "cpp", "cuda", "cu", "objc", "objcpp", "proto"},
}
lsp.cmake.setup{}

lsp.bashls.setup{
    on_attach = lsp_status.on_attach,
    capabilities = lsp_status.capabilities,
}

lsp.texlab.setup{
    cmd = {'/home/samuel/programs/texlab/target/release/texlab'}
}

lsp.rust_analyzer.setup{
    on_attach = lsp_status.on_attach,
    capabilities = capabilities,
    cmd = {'/home/samuel/.local/share/rustup/toolchains/nightly-x86_64-unknown-linux-gnu/bin/rust-analyzer'}
}

lsp.fortls.setup{
    on_attach = lsp_status.on_attach,
    capabilities = lsp_status.capabilities,
}

lsp.marksman.setup{
    root_dir = lsp.util.root_pattern('.git', 'markdown.toml', 'Notes')
}

lsp.lua_ls.setup {
    on_init = function(client)
        local path = client.workspace_folders[1].name
        if vim.loop.fs_stat(path..'/.luarc.json') or vim.loop.fs_stat(path..'/.luarc.jsonc') then
            return
        end

        client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
            runtime = {
                -- Tell the language server which version of Lua you're using
                -- (most likely LuaJIT in the case of Neovim)
                version = 'LuaJIT'
            },
            -- Make the server aware of Neovim runtime files
            workspace = {
                checkThirdParty = false,
                library = {
                    vim.env.VIMRUNTIME
                    -- Depending on the usage, you might want to add additional paths here.
                    -- "${3rd}/luv/library"
                    -- "${3rd}/busted/library",
                }
                -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
                -- library = vim.api.nvim_get_runtime_file("", true)
            }
        })
    end,
    settings = {
        Lua = {}
    }
}

-- Load built-in spell checker dictionary, taken from: https://www.reddit.com/r/neovim/comments/s24zvh/how_can_i_load_a_user_dictionary_into_ltexls/
local path = vim.fn.stdpath("config") .. "/spell/en.utf-8.add"
local words = {}

local file = io.open(path, "r")
if file ~= nil then
    for word in file:lines() do
        table.insert(words, word)
    end
end

lsp.ltex.setup{
    root_dir = lsp.util.root_pattern('.git', 'markdown.toml', 'Notes'),
    settings = {
        ltex = {
            language = "en-US",
            -- dictionary = {["en-GB"] = {'AccessNode', 'DaCe', 'cloudsc', 'memlet', 'CUDA', 'NVIDIA', 'speedup'}},
            dictionary = {["en-GB"] = words, ["en-US"] = words},
            enabled = {'bibtex', 'context', 'contex.tex', 'html', 'latex', 'restructuredtext', 'tex'},
            enabledRules = {
                ["en-GB"] = {"OXFORD_SPELLING_NOUNS"}
            },
            disabledRules = {
                ["en-GB"] = {"OXFORD_SPELLING_Z_NOT_S"}
            }
        },
    },
}

-- lsp.proselint.setup{
-- }
--
lsp.phpactor.setup{}
