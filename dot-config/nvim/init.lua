local utils = require('utils')

local global = vim.o -- For the global options
local window = vim.wo -- For the window local options
local buffer = vim.bo -- For the buffer local options

vim.g.mapleader = ' '
vim.g.maplocalleader = ','

-- TODO: config spell checking
-- https://github.com/lewis6991/spellsitter.nvim
-- https://jdhao.github.io/2019/04/29/nvim_spell_check/
-- And maybe add proselint
-- buffer.spelllang = en

vim.opt.spelllang = 'en_gb'
vim.opt.spell = true
vim.opt.conceallevel = 0
vim.opt.concealcursor = 'nc'
vim.opt.termguicolors = true
vim.opt.colorcolumn = '120'

window.colorcolumn = '120'
window.nu = true
window.relativenumber = true
window.list = true

global.softtabstop = 4
global.shiftwidth = 4
global.tabstop = 4
global.expandtab = true
global.smartindent = true
global.tw = 120
global.syntax = "enable"
global.showmatch = true
global.ignorecase = true
global.smartcase = true
global.autoread = true
global.updatetime = 500
global.timeout = true
global.timeoutlen = 500
global.showtabline = 2


-- Folding
window.foldmethod = "expr"
window.foldexpr = "nvim_treesitter#foldexpr()"
-- disable folding at startup
window.foldenable = false

global.clipboard = [[unnamed,unnamedplus]]

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    { import = "plugins"},
    { import = "plugins.languages"}
})

-- Appearance
vim.cmd('colorscheme catppuccin')
vim.g.neosolarized_vertSplitBgTrans = 1
vim.o.cursorline = true


require('keymap')
require('lsp')
require('statusline')
require('custom_functions')
require('autocommands')

-- Completion
vim.g.completion_enable_auto_popup = 0
vim.g.completion_enable_auto_signature = 0
global.completeopt = 'menuone,noinsert,noselect'

if vim.g.neovide then
    require('neovide')
end
