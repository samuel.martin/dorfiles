local dpi = beautiful.xresources.apply_dpi
local theme = beautiful.get()

local hwImage = wibox.widget.imagebox()
local hwArcChart = wibox.container.arcchart(hwImage)
hwArcChart.min_value    = 0
hwArcChart.max_value    = 100
hwArcChart.thickness    = dpi(6)
hwArcChart.paddings     = dpi(10)
hwArcChart.border_width = dpi(0)
hwArcChart.visible      = true
hwArcChart.colors       = {theme.fg_normal}
hwArcChart.bg           = theme.bg_focus
local hwPopup = awful.popup({
	maximum_height = dpi(80),
	maximum_width  = dpi(80),
	ontop          = true,
	placement      = awful.placement.bottom,
	shape          = gears.shape.rounded_bar,
	hide_on_right_click = true,
	visible        = false,
	opacity        = 0.8,
	widget         = hwArcChart
	})

local hwTimer = gears.timer{
	timeout     = 5,
	autostart   = false,
	call_now    = false,
	callback    = function() hwPopup.visible = false end,
	single_shot = false,
}

local function show_hw_popup()
    hwPopup.visible = true
    if hwTimer.started then
        hwTimer:again()
    else
        hwTimer:start()
    end
end

function MyPopups.brightness_popup(brightness)
    hwImage.image = recolorIcon("brightness-7")
    hwArcChart.value = brightness
    show_hw_popup()
end

function MyPopups.volume_popup(volume)
    hwImage.image = recolorIcon("volume-medium")
    hwArcChart.value = volume
    show_hw_popup()
end
local theme = beautiful.get()
