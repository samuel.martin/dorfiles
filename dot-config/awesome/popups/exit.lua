local theme = beautiful.get()
local dpi = beautiful.xresources.apply_dpi

local exitContainer = wibox.widget {
	homogeneous   = true,
	min_cols_size = 10,
	min_rows_size = 10,
	layout        = wibox.layout.grid,
	spacing       = dpi(20),
}

local exitSettings = {}
exitSettings.icon_size = 50

local reboot_icon = wibox.widget {
	image = recolorIcon('restart'),
	resize = true,
	forced_height = dpi(exitSettings.icon_size),
	forced_width = dpi(exitSettings.icon_size),
	widget = wibox.widget.imagebox,
}

local power_icon = wibox.widget {
	image = recolorIcon('power'),
	resize = true,
	forced_height = dpi(exitSettings.icon_size),
	forced_width = dpi(exitSettings.icon_size),
	widget = wibox.widget.imagebox,
}

local lock_icon = wibox.widget {
	image = recolorIcon('lock'),
	resize = true,
	forced_height = dpi(exitSettings.icon_size),
	forced_width = dpi(exitSettings.icon_size),
	widget = wibox.widget.imagebox,
}

local suspend_icon = wibox.widget {
	image = recolorIcon('power-sleep'),
	resize = true,
	forced_height = dpi(exitSettings.icon_size),
	forced_width = dpi(exitSettings.icon_size),
	widget = wibox.widget.imagebox,
}


local reboot_lbl = wibox.widget.textbox('(R)eboot')
local power_lbl = wibox.widget.textbox('(P)ower Off')
local lock_lbl = wibox.widget.textbox('(L)ock')
local suspend_lbl = wibox.widget.textbox('(S)uspend')


local reboot_container = wibox.widget {
{
        {
            {
                reboot_icon,
                reboot_lbl,
                layout = wibox.layout.fixed.vertical
            },
            margins = dpi(10),
            widget = wibox.container.margin,
        },
        shape = gears.rounded_rect,
        shape_border_color = theme.fg_normal,
        shape_border_width = dpi(10),
        shape_cli = true,
        widget = wibox.container.background,
    },
    margins = dpi(10),
    forced_height = dpi(100),
    forced_width = dpi(100),
    widget = wibox.container.margin,
}

local power_container = wibox.widget {
{
        {
            {
                power_icon,
                power_lbl,
                layout = wibox.layout.fixed.vertical
            },
            margins = dpi(10),
            widget = wibox.container.margin,
        },
        shape = gears.rounded_rect,
        shape_border_color = theme.fg_normal,
        shape_border_width = dpi(10),
        shape_cli = true,
        widget = wibox.container.background,
    },
    margins = dpi(10),
    forced_height = dpi(100),
    forced_width = dpi(100),
    widget = wibox.container.margin,
}

local lock_container = wibox.widget {
{
        {
            {
                lock_icon,
                lock_lbl,
                layout = wibox.layout.fixed.vertical
            },
            margins = dpi(10),
            widget = wibox.container.margin,
        },
        shape = gears.rounded_rect,
        shape_border_color = theme.fg_normal,
        shape_border_width = dpi(10),
        shape_cli = true,
        widget = wibox.container.background,
    },
    margins = dpi(10),
    forced_height = dpi(100),
    forced_width = dpi(100),
    widget = wibox.container.margin,
}

local suspend_container = wibox.widget {
{
        {
            {
                suspend_icon,
                suspend_lbl,
                layout = wibox.layout.fixed.vertical
            },
            margins = dpi(10),
            widget = wibox.container.margin,
        },
        shape = gears.rounded_rect,
        shape_border_color = theme.fg_normal,
        shape_border_width = dpi(10),
        shape_cli = true,
        widget = wibox.container.background,
    },
    margins = dpi(10),
    forced_height = dpi(100),
    forced_width = dpi(100),
    widget = wibox.container.margin,
}

local function container_hover_in(find_widgets_result)
	find_widgets_result.widget.bg = theme.bg_focus
end

local function container_hover_out(find_widgets_result)
	find_widgets_result.widget.bg = theme.bg_normal
end

local exit_keygrabber = awful.keygrabber {
	stop_key = 'Escape'
}
local function power_off()
	exit_keygrabber:stop()
	awful.spawn.with_shell('systemctl poweroff')
end

local function restart()
	exit_keygrabber:stop()
	awful.spawn.with_shell('systemctl reboot')
end

local function lock()
	exit_keygrabber:stop()
	awful.spawn.with_shell("lock "..theme.wallpaper)
end

local function suspend()
    exit_keygrabber:stop()
    lock()
    awful.spawn.with_shell("systemctl suspend")
end

exit_keygrabber:add_keybinding({}, 'p', power_off)
exit_keygrabber:add_keybinding({}, 'r', restart)
exit_keygrabber:add_keybinding({}, 'l', lock)
exit_keygrabber:add_keybinding({}, 's', suspend)


exitContainer:add_widget_at(reboot_container, 1,1, 1, 1)
reboot_container:connect_signal('mouse::enter', container_hover_in)
reboot_container:connect_signal('mouse::leave', container_hover_out)
reboot_container:connect_signal('button::press', restart)

exitContainer:add_widget_at(power_container, 1, 2, 1, 1)
power_container:connect_signal('mouse::enter', container_hover_in)
power_container:connect_signal('mouse::leave', container_hover_out)
power_container:connect_signal('button::press', power_off)

exitContainer:add_widget_at(lock_container, 1, 3, 1, 1)
lock_container:connect_signal('mouse::enter', container_hover_in)
lock_container:connect_signal('mouse::leave', container_hover_out)
lock_container:connect_signal('button::press', lock)

exitContainer:add_widget_at(suspend_container, 1, 4, 1, 1)
suspend_container:connect_signal('mouse::enter', container_hover_in)
suspend_container:connect_signal('mouse::leave', container_hover_out)
suspend_container:connect_signal('button::press', suspend)

local exitPopup = awful.popup{
	widget         =
	{
    	{
        	exitContainer,
        	widget = wibox.container.background,
        	shape = gears.shape.rounded_rect,
        	shape_border_width = dpi(5),
        	shape_border_color = theme.fg_normal,
        	shape_clip = false,
    	},
    	margins = dpi(3),
    	widget = wibox.container.margin,
    },
	maximum_height = dpi(300),
	maximum_width  = dpi(500),
	ontop          = true,
	placement      = awful.placement.centered,
	shape          = gears.shape.rounded_rectangle,
	hide_on_right_click = false,
	visible        = false,
	opacity        = 0.8,
}


function MyPopups.exit_popup()
    exit_keygrabber:start()
    exitPopup.visible = true
end

function MyPopups.exit_popup_close()
    exitPopup.visible = false
end

exit_keygrabber:connect_signal('stopped', function() MyPopups.exit_popup_close() end)
