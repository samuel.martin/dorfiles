local theme = beautiful.get()
local dpi = beautiful.xresources.apply_dpi

local screen_popup_container = wibox.layout.fixed.vertical()
local screen_popup = awful.popup {
	widget = screen_popup_container,
	maximum_height = dpi(500),
	maximum_width = dpi(500),
	placement = awful.placement.centered,
	visible = false,
}
local screen_keygrabber = awful.keygrabber{
	stop_key = 'Escape'
}
function MyPopups.create_screen_popup()
	for s in screen do
		-- for key, value in pairs(s.outputs) do
			-- if value ~= nil then
				-- print(key..': '..value)
			-- else
				-- print(key..': nil')
			-- end
		-- end
		screen_popup_container:add(wibox.widget {
				text = ': '..s.geometry['x']..'x'..s.geometry['y']..' of size '..s.geometry['width']..'x'..s.geometry['height'],
				widget = wibox.widget.textbox,
			})
	end
end

function MyPopups.screen_popup()
	screen_keygrabber:start()
	screen_popup.visible = true
end

function MyPopups.screen_popup_close()
	screen_popup.visible = false
end

screen_keygrabber:connect_signal('stopped', function() MyPopups.screen_popup_close() end)

