local theme = beautiful.get()
local dpi = beautiful.xresources.apply_dpi

require ("widgets.sliderbar")

local container = wibox.widget {
	homogeneous   = true,
	min_cols_size = 10,
	min_rows_size = 10,
	layout        = wibox.layout.grid,
	spacing       = dpi(20),
}

local brigthness_slider = wibox.widget {
	bar_shape = gears.shape.rounded_rect,
	bar_height = dpi(10),
	-- bar_color = theme.bg_focus,
	bar_color = theme.transparent_color,
	handle_color = theme.fg_focus,
	handle_shape = gears.shape.circle,
	handle_width = dpi(30),
	value = 50,
	max_value = 100,
	widget = wibox.widget.slider,
}

local brigthness_bar = wibox.widget {
	value = 50,
	max_value = 100,
	shape = gears.shape.rounded_rect,
	background_color = theme.bg_focus,
	color = theme.fg_focus,
	forced_height = dpi(10),
	widget = wibox.widget.progressbar
}

local brightness = wibox.widget {
	brigthness_bar,
	brigthness_slider,
	forced_height = dpi(30),
	layout = wibox.layout.stack
}

slb = Sliderbar:create(dpi(400), dpi(30))
slb:set_value(30)
container:add_widget_at(slb.container, 1, 1, 1, 1)
container:add_widget_at(brightness, 2, 1, 1, 1)

local popup = awful.popup {
	widget = container,
	maximum_width = dpi(600),
	maximum_height = dpi(600),
	ontop = true,
	placement = awful.placement.centered,
	shape = gears.shape.rounded_rectangle,
	hide_on_right_click = false,
	visible = false,
}


function MyPopups.control_popup()
	-- popup.visible = true
end

function MyPopups.control_popup_exit()
	popup.visible = false
end
