groups = {
l="launcher",
c="client",
a="awesome",
s="screen",
t="tag",
h="hardware"
}

special_keys = {
M=modkey,
S="Shift",
C="Control",
}


MyPopups = require('popups')

function key(keys, action, description, group)
    k = keys[#keys]
    modifiers = {}
    for i=1, #keys-1 do
        if special_keys[keys[i]] ~= nil then
            modifiers[i] = special_keys[keys[i]]
        end
    end

    group = groups[group]

    return awful.key(modifiers, k, action,
                    {description=description, group=group})
end

function brightness_up()
    awful.spawn("brightnessctl set 2%+")
    vicious.force(screen_brightness_observer_widgets)
    -- vicious.call_async(vicious.widgets.brightness, "$1", nil,
    -- 	function(brightness) MyPopups.brightness_popup(brightness) end)
    vicious.widgets.brightness.async(nil, nil, function(t)
        MyPopups.brightness_popup(t[1])
    end)
end

function brightness_down()
    awful.spawn("brightnessctl set 2%-")
    vicious.force(screen_brightness_observer_widgets)
    vicious.widgets.brightness.async(nil, nil, function(t)
        MyPopups.brightness_popup(t[1])
    end)
end

function kbd_brightness_up()
    awful.spawn("brightnessctl -d smc::kbd_backlight set 10%+")
    -- awful.spawn("notify-send 'Keyboard Brightness Up'")
end

function kbd_brightness_down()
    awful.spawn("brightnessctl -d smc::kbd_backlight set 10%-")
    -- awful.spawn("notify-send 'Keyboard Brightness Down'")
end

 -- {{{ Key bindings
globalkeys = gears.table.join(
	key({"M", "e"},
    	function()
            for i =1,#root.tags() do
                tag = root.tags()[i]
                -- naughty.notify({text="id: "..tag.id..", name: "..tag.name})
                naughty.notify({text="name: "..tag.name})
            end
    	end, "", "h"),
    	

	-- Popups
	key({"M","s"}, function() hotkeys_popup.show_help(nil, awful.screen.focused()) end,
	    "show help","a"),

	key({"M", "Escape"}, function() MyPopups.exit_popup() end,
		"show exit controls", "h"),
    key({"M", "c"}, function() MyPopups.control_popup() end,
        "Show control popup", "h"),
	-- Window management
    key({"M", "j"}, function() awful.client.focus.bydirection("down") end,
        "focus window below", "c"),
    key({"M", "k"}, function() awful.client.focus.bydirection("up") end,
        "focus window above", "c"),
    key({"M", "l"}, function() awful.client.focus.bydirection("right") end,
        "focus right window", "c"),
    key({"M", "h"}, function() awful.client.focus.bydirection("left") end,
        "focus left window", "c"),

    -- Layout manipulation
    key({"M", "S", "j"}, function() awful.client.swap.bydirection("down") end,
        "swap with client below", "c"),
    key({"M", "S", "k"}, function() awful.client.swap.bydirection("up") end,
        "swap with client above", "c"),
    key({"M", "S", "l"}, function() awful.client.swap.bydirection("right") end,
        "swap with right client", "c"),
    key({"M", "S", "h"}, function() awful.client.swap.bydirection("left") end,
        "swap with left client", "c"),
        
    key({"M", "C", "l"}, function() awful.tag.incmwfact(.05) end,
        "mover border to the right", "c"),
    key({"M", "C", "h"}, function() awful.tag.incmwfact(-.05) end,
         "move border to the left", "c"),
        
        
    key({"M", "space"}, function() awful.layout.inc( 1) end,
        "select next", "l"),
    key({"M","S", "space"}, function() awful.layout.inc(-1) end,
        "select previous", "l"),

    -- Screen
    key({"M", "u"}, function() awful.screen.focus_relative(1) end,
        "focus the next screen", "s"),
    key({"M", "i"}, function() awful.screen.focus_relative(-1) end,
        "focus the previous screen", "s"),
        
    key({"M", "S", "u"},
        function()
        	tag = awful.screen.focused().selected_tags[1]
        	awful.screen.focus_relative(1)
        	move_tag_to_screen(tag, awful.screen.focused())
    	end,
        "move current tag to the next screen", "s"),
    key({"M", "S","i"},
        function()
        	tag = awful.screen.focused().selected_tags[1]
        	awful.screen.focus_relative(-1)
        	move_tag_to_screen(tag, awful.screen.focused())
    	end,
        "move current tag to the previous screen", "s"),

    -- Standard program
    key({"M", "Return"}, function() awful.spawn(terminal) end,
        "open a terminal", "l"),
    key({"M", "C", "r"}, awesome.restart,
        "restart awesome", "a"),
    key({"M","m"}, function() awful.spawn("rofi -show combi") end,
        "start application launcher", "l"),
    key({"M","d"}, function() awful.spawn.with_shell("sh ~/scripts/xmonitors") end,
        "start screen configuration", "l"),
    key({"M","n"}, function() awful.spawn.with_shell("sh ~/scripts/wifi-chooser") end,
        "start wifi chooser", "l"),
    key({"M", "w"}, function() MyPopups.screen_popup() end,
        "List monitors", "l"),

	-- Special keys
    key({"Print"}, function() awful.spawn.with_shell('~/scripts/screenshot.sh') end, "Make Screenshot", "h"),
    key({"S", "Print"}, function() awful.spawn.with_shell('~/scripts/area_screenshot.sh') end, "Make Screenshot of an area", "h"),
    key({"XF86MonBrightnessUp"}, brightness_up,
    	"increase brightness", "h"),
    key({"XF86MonBrightnessDown"}, brightness_down,
    	"decrease brightness", "h"),
    key({"XF86KbdBrightnessUp"}, kbd_brightness_up,
    	"increase keyboard brightness", "h"),
    key({"XF86KbdBrightnessDown"}, kbd_brightness_down,
    	"decrease keyboard brightness", "h"),
    key({"XF86AudioLowerVolume"}, function()
        	awful.spawn.with_shell("amixer set 'Master' 5%-")
        	vicious.force(audio_observer_widgets)
        	vicious.call_async(vicious.widgets.volume, "$1", "Master",
        		function(volume) MyPopups.volume_popup(volume) end)
            -- vicious.widgets.volume.async(nil, "Master",
            --     function(t)
            --         MyPopups.volume_popup(t[1])
            --     end
            -- )
    	end,
    	"decrease volume", "h"),
    key({"XF86AudioRaiseVolume"}, function()
        	awful.spawn.with_shell("amixer set 'Master' 5%+")
        	vicious.force(audio_observer_widgets)
        	vicious.call_async(vicious.widgets.volume, "$1", "Master",
        		function(volume) MyPopups.volume_popup(volume) end)
            -- vicious.widgets.volume.async(nil, "Master",
            --     function(t)
            --         MyPopups.volume_popup(t[1])
            --     end
            -- )
    	end,
    	"increase volume", "h"),

    key({"XF86AudioMute"}, function()
        	awful.spawn.with_shell("amixer set 'Master' 'toggle'")
        	-- seems to be the case that we need to wait a little bit, until the mute is observable
        	os.execute("sleep .01")
        	vicious.force(audio_observer_widgets)
    	end,
    	"increase volume", "h"),

  	key({"XF86Eject"}, function() awful.spawn.with_shell("eject") end,
  		"eject CD drive", "h"),
    
    -- Prompt
    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"})

)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    key({"M", "S", "q"}, function(c) c:kill() end, "test", "c"),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
    
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    -- awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
    --           {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 10 do
    globalkeys = gears.table.join(globalkeys,
            -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                      select_tag(root.tags()[i])
                   end,
                  {description = "view tag #"..i, group = "tag"}),

        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = root.tags()[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"})
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)

-- Set keys
root.keys(globalkeys)

-- }}}
 
-- {{{ unused keybindings, function calls might be usefull
    -- awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
    --           {description = "view previous", group = "tag"}),
    -- awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
    --           {description = "view next", group = "tag"}),
    -- awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
    --           {description = "go back", group = "tag"}),
    -- awful.key({ modkey,           }, "Tab",
    --     function ()
    --         awful.client.focus.history.previous()
    --         if client.focus then
    --             client.focus:raise()
    --         end
    --     end,
    --     {description = "go back", group = "client"}),
    -- awful.key({ modkey, "Control"   }, "q", awesome.quit,
    --           {description = "quit awesome", group = "awesome"}),
    -- awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
    --           {description = "increase the number of master clients", group = "layout"}),
    -- awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
    --           {description = "decrease the number of master clients", group = "layout"}),
    -- awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
    --           {description = "increase the number of columns", group = "layout"}),
    -- awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
    --           {description = "decrease the number of columns", group = "layout"}),
    -- awful.key({ modkey, "Control" }, "n",
    --           function ()
    --               local c = awful.client.restore()
    --               -- Focus restored client
    --               if c then
    --                 c:emit_signal(
    --                     "request::activate", "key.unminimize", {raise = true}
    --                 )
    --               end
    --           end,
    --           {description = "restore minimized", group = "client"}),
    -- Menubar
    -- awful.key({ modkey }, "p", function() menubar.show() end,
    --           {description = "show the menubar", group = "launcher"})
        -- Toggle tag on focused client.
        -- awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
        --           function ()
        --               if client.focus then
        --                   local tag = client.focus.screen.tags[i]
        --                   if tag then
        --                       client.focus:toggle_tag(tag)
        --                   end
        --               end
        --           end,
                  -- {description = "toggle focused client on tag #" .. i, group = "tag"})
        -- Toggle tag display.
        -- awful.key({ modkey, "Control" }, "#" .. i + 9,
        --           function ()
        --               local screen = awful.screen.focused()
        --               local tag = screen.tags[i]
        --               if tag then
        --                  awful.tag.viewtoggle(tag)
        --               end
        --           end,
        --           {description = "toggle tag #" .. i, group = "tag"}),
-- }}}
         
