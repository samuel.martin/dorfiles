-- Autorun
autorunApps = {
	"mpd",
	"sh ~/scripts/start_redshift",
	"dropbox start",
    "xinput set-prop '"..hwconfig.xinput_touchpad.."' 'libinput Natural Scrolling Enabled' 1",
	"setxkbmap -layout ch -option 'caps:swapescape,altwin:swap_lalt_lwin'",
	"xrandr --output "..hwconfig.xrandr_laptop_screen.." --mode "..hwconfig.xrandr_laptop_resolution,
}

for app = 1, #autorunApps do
    awful.spawn.with_shell(autorunApps[app])
end
