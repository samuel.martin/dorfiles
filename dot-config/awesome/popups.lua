local theme = beautiful.get()
local dpi = beautiful.xresources.apply_dpi

MyPopups = {}

require("popups/hw-popups")
require("popups/exit")
require("popups/screen")
require("popups/control")

return MyPopups
