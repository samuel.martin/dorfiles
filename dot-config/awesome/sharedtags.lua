-- collection of methods so simulate shared/global tags

-- table of all tags
local mytags = {
      {name="1"}
    , {name="2"}
    , {name="3"}
    , {name="4"}
    , {name="5"}
    , {name="6"}
    , {name="7"}
    , {name="8"}
    , {name="9"}
    , {name="0"}
}

-- function which creates all the tags, can be called multiple times, will only
-- create the tags, if the haven't been created. If the have already been
-- created moves the first tag in the table to that screen
function init_tags(screen)
    if #root.tags() == 0 then
        for i = 1, #mytags do
            awful.tag.add(
            mytags[i].name,
            {
                layout = awful.layout.suit.tile,
                screen = screen
            })
        end

        root.tags()[1]:view_only()
    else
        -- todo: do better
        select_tag(root.tags()[2], screen)
            
    end
end

-- moves focus to the given tag, my switch focus to another screen
function select_tag(tag)
    awful.screen.focus(tag.screen)
    tag:view_only()
end

-- moves the given tag to the given screen and focus it there
function move_tag_to_screen(tag, screen)
    tag.screen = screen
    tag:view_only()
    
end

function get_tags_list(screen, args)
    return root.tags()
end
