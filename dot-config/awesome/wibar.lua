local theme = beautiful.get()
local dpi = beautiful.xresources.apply_dpi


-- {{{ Wibar

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- Create a textclock widget
mytextclock = wibox.widget.textclock()

local batteryicon = wibox.widget.imagebox()
local iconname = "battery"
local battery = wibox.widget.textbox()
vicious.register(battery, vicious.widgets.bat,
	function(widget, args)
    	battery_per = args[2]
    	charging = args[1] =='↯' or args[1] == '+'
    	iconname = "battery-"..math.floor(battery_per/10)*10
    	critical = battery_per < 30


    	if battery_per == 100 and not charging then
        	iconname = "battery"
    	elseif battery_per == 100 and charging then
        	iconname = "battery-charging-100"
    	elseif charging then
        	iconname = "battery-charging-"..math.ceil(battery_per/10)*10
    	end

    	if not critical then
        	setIcon(batteryicon, iconname)
    	else
        	setIcon(batteryicon, iconname, beautiful.get().fg_urgent)
    	end
    	return args[2].."%"
	end
    , 37, "BAT0")


-- Createm a rotate progress bar used as one bar in the cpu bar plot
function create_cpu_bar(value)
    bar = wibox.widget {
        { widget = wibox.widget.progressbar,
            max_value = 100,
            value = value,
            paddings = dpi(1),
            color = theme.fg_normal,
            background_color = theme.bg_normal,
        },
        direction = 'east',
        forced_width = dpi(10),
        layout = wibox.container.rotate,
    }
    return bar
end

-- Format the cpu bars -> update values
-- also create one bar for each cpu if called the first time
function format_cpu_bar(widget, args)
    if #widget:get_all_children() == 0 then
        -- first time this is called -> need to create progressbars
        for i=2, #args do
            widget:add(create_cpu_bar(args[i]))
        end
    else
        -- get the direct children which are the wibox.container.rotate
        local children = widget:get_children()
        for i=2, #args do
            -- get the children of the rotate container which is the prograss bar
            children[i-1]:get_children()[1]:set_value(args[i])
        end
    end
end


local cpu_bar_container = wibox.layout.fixed.horizontal()

vicious.register(cpu_bar_container, vicious.widgets.cpu, format_cpu_bar, 3)


local memorywidget = wibox.widget.textbox()
vicious.cache(vicious.widgets.mem)
vicious.register(memorywidget, vicious.widgets.mem, "$2 MiB ($1%) ", 13)
local memoryicon = wibox.widget.imagebox()
memoryicon.image = gears.color.recolor_image(
    "/home/samuel/.local/share/material-design-icons/svg/memory.svg",
    beautiful.get().fg_normal
)

local fshome = wibox.widget.textbox()
local fshomeicon = wibox.widget.imagebox()
fshomeicon.image = gears.color.recolor_image(
	"/home/samuel/.local/share/material-design-icons/svg/folder-home.svg",
	beautiful.get().fg_normal
)
vicious.cache(vicious.widgets.fs)
vicious.register(fshome, vicious.widgets.fs, "${/ avail_gb} GiB ", 33)

local fsdata = wibox.widget.textbox()
local fsdataicon = wibox.widget.imagebox()
fsdataicon.image = gears.color.recolor_image(
	"/home/samuel/.local/share/material-design-icons/svg/harddisk.svg",
	beautiful.get().fg_normal
)
vicious.register(fsdata, vicious.widgets.fs, "${/mnt/data avail_gb} GiB ", 33)

local thermometericon = wibox.widget.imagebox()
thermometericon.image = gears.color.recolor_image(
	"/home/samuel/.local/share/material-design-icons/svg/thermometer.svg",
	beautiful.get().fg_normal
)
local cputemp = wibox.widget.textbox()
vicious.cache(vicious.widgets.hwmontemp)
vicious.register(cputemp, vicious.widgets.hwmontemp, "$1 C° ", 7, {"pch_skylake"})

local gputemp = wibox.widget.textbox()
vicious.register(gputemp, vicious.widgets.hwmontemp, "$1 C° ", 7, {"amdgpu"})

local volume = wibox.widget.textbox()
local volumeicon = wibox.widget.imagebox()
local iconname = "volume-medium"
vicious.cache(vicious.widgets.volume)
vicious.register(volume, vicious.widgets.volume,
	function(widget, args)
		local volume_per = args[1]

		if volume_per > 59 then
    		iconname = "volume-high"
    	elseif volume_per > 20 then
        	iconname = "volume-medium"
		elseif volume_per > 0 then
    		iconname = "volume-low"
		end
		if args[2] ~= '🔉' then
    		iconname = "volume-mute"
		end
		setIcon(volumeicon, iconname)
    	return args[1].."% "

	end
    , 67, "Master")
table.insert(audio_observer_widgets, volume)

local brightness = wibox.widget.textbox()
vicious.register(brightness, vicious.widgets.brightness, "$1% ", 61)
vicious.cache(vicious.widgets.brightness)
local brightnessicon = wibox.widget.imagebox()
setIcon(brightnessicon, "brightness-7")
table.insert(screen_brightness_observer_widgets, brightness)

local wifi = wibox.widget.textbox()
vicious.register(wifi, vicious.widgets.wifi, "${ssid} (${linp}%)", 61, "wlo1")
vicious.cache(vicious.widgets.wifi)
local wifi_icon = wibox.widget.imagebox()
setIcon(wifi_icon, "wifi")

-- Create tags and taglist
local taglist_buttons = gears.table.join(
      awful.button({ }, 1, function(t) select_tag(t, awful.screen.focused()) end)
)

-- creates the wibar for the given screen s
function create_wibar(s)
    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
                           
    s.mytaglist = awful.widget.taglist({
       screen = s,
       buttons = taglist_buttons,
       filter = get_tags_list
       -- filter = function(screen, args) return root.tags() end,
       -- filter = awful.widget.taglist.filter.all
    })
    
    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        expand = "none",
        -- expand = "inside",
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            fsdataicon, fsdata,
            fshomeicon, fshome,
            s.mypromptbox,
        },
        { -- Middle widget
            layout = wibox.layout.fixed.horizontal,
            mytextclock,
    	},
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            brightnessicon, brightness,
            volumeicon, volume,
            thermometericon, cputemp, gputemp,
            wifi_icon, wifi,
            memoryicon, memorywidget,
            cpu_bar_container,
            batteryicon, battery,
            wibox.widget.systray(),
            s.mylayoutbox,
        },
    }
end
