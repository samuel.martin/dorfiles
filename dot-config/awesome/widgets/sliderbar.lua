local theme = beautiful.get()


Sliderbar = {}
Sliderbar.__index = Sliderbar

function Sliderbar:create(width, height, max_value)
    local slb = {}
    setmetatable(slb, Sliderbar)

    slb.max_value = max_value or 100
    local bar_height = height/3

    slb.bar = wibox.widget {
        value = 0,
        max_value = slb.max_value,
        shape = gears.shape.rounded_rect,
        background_color = theme.bg_focus,
        color = theme.fg_focus,
        forced_height = bar_height,
        widget = wibox.widget.progressbar
    }

    slb.slider = wibox.widget {
        value = 0,
        max_value = slb.max_value,
        bar_shape = gears.shape.rounded_rect,
        bar_height = bar_height,
        bar_color = theme.transparent_color,
        handle_color = theme.fg_focus,
        handle_shape = gears.shape.circle,
        handle_width = height,
        widget = wibox.widget.slider,
    }

    slb.container = wibox.widget {
        bar,
        slider,
        forced_height = height,
        layout = wibox.layout.stak
    }

    return slb

end

function Sliderbar:set_value(value)
    self.bar.value = value
    self.slider.value = value
end
