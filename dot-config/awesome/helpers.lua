--- Function to set the image of a given widget to the given icon
--  as they are in ~/.local/share/material-design-icons/svg/
--  adds .svg ending to icon name
--  @param widget The imagewidget
--  @param iconname The name of the icon
--  @param[opt=beautiful.get().fg_normal] color Color of the icon
function setIcon(widget, iconname, color)
    -- color = color or beautiful.get().fg_normal
    -- widget.image = gears.color.recolor_image("/home/samuel/.local/share/material-design-icons/svg/"..iconname..".svg", color)
    widget.image = recolorIcon(iconname, color)
end

--- Returns the recolored icon. The iconname must be the file located in
-- ~/.local/share/material-design-icons/svg/ without the ending .svg
-- @param iconname The name of the icon
--  @param[opt=beautiful.get().fg_normal] color Color of the icon
function recolorIcon(iconname, color)
    color = color or beautiful.get().fg_normal
    return gears.color.recolor_image("/home/samuel/.local/share/material-design-icons/svg/"..iconname..".svg", color)
end
