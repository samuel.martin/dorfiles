#!/bin/bash

####################################
### Remove disconnected monitors ###
####################################

laptop_monitor="eDP-1"
# get monitors where xrandr says that they are connected and bspc as a monitor entry for it
connected_monitors=($(echo -e "$(bspc query -M --names)\n$(xrandr | awk '/ connected/ {print $1}')" | sort | uniq -d))

for monitor in $(bspc query -M --names); do
    # Check if the bspc monitor is not connected anymore
    if ! [[ $(xrandr | awk '/ connected/ {print $1}') =~ $monitor ]]; then
        # transfer all non-temp desktops to the laptop monitor
        for desktop in $(bspc query -D -m $monitor --names | awk '/^[0-9]/ {print $0}'); do
            bspc desktop $desktop --to-monitor $laptop_monitor
        done
        # remove monitor
        bspc monitor $monitor --remove
        xrandr --output $monitor --off
    fi
done
