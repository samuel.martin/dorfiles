#!/bin/bash
# Starts the polybar on the monitor given as the first argument

MONITOR=$1 nohup polybar --config=~/.config/polybar/bspwm $2&
