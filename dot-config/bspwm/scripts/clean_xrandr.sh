#!/bin/bash
# Removes all outputs from xrandr which are disconnected but still have a mode (resolution)

# for all monitors which are disconnected but xrandr has still a resolution
for monitor in $(xrandr | awk '/ disconnected [0-9]*x[0-9]*/ {print $1}'); do
    # disconnect the monitor
    xrandr --output $monitor --off
done;

