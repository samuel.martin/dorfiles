#!/bin/bash

for pid in $(pidof polybar); do
    # Get the monitor the polybar instance runs on, this was given as an environment variable
    monitor=$(strings /proc/$pid/environ | awk '/MONITOR/ {split($0,a,"="); print a[2]}')
    # if the monitor is not connected anymore, kill it
    echo $pid $monitor
    if ! [[ $(xrandr | awk '/ connected/ {print $1}') =~ $monitor ]]; then
        kill  $pid
    fi
done
