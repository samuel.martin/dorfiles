# source plugin manager
source "%val{config}/plugins/plug.kak/rc/plug.kak"

#add kak-lsp
#plug "ul/kak-lsp" do %{
#    cargo install --locked --force --path .
#}

# add powerline plugin
plug "andreyorst/powerline.kak" defer powerline %{
    powerline-theme solarized-light
} config %{
    powerline-start
}

# add fuzzy file finder (fzf) plugin
plug "andreyorst/fzf.kak" defer "fzf" %{
}


# add smarttab plugin
plug "andreyorst/smarttab.kak"
hook global WinSetOption filetype=(c|cpp) expandtab

# add tagbar plugin
plug "andreyorst/tagbar.kak" defer "tagbar" %{
    set-option global tagbar_sort false
    set-option global tagbar_size 40
    set-option global tagbar_display_anon false
} config %{
     # if you have wrap highlighter enabled in you configuration
     # files it's better to ruen it off for tagbar using this hook:
     hook global WinSetOption filetype=tagbar %{
         remove-highlighter window/wrap
         # you can also disable rendering whitespaces here, line numbers, and
         # matching characters
         }
}

# add rainbow plugin (highlights matching paranthesis)
plug 'jjk96/kakoune-rainbow'

# add plugin to automatically yank to system clipboard
plug 'lePerdu/kakboard' %{
        hook global WinCreate .* %{ kakboard-enable }
}

# plugin to automatically add closing brackets
plug 'alexherbo2/auto-pairs.kak'

#
plug "andreyorst/kaktree" config %{
        # remove highglighters not needes for kaktree	
    hook global WinSetOption filetype=kaktree %{
        remove-highlighter buffer/number-lines
        remove-highlighter buffer/matching
        remove-highlighter buffer/wrap
        remove-highlighter buffer/show-whitespaces
        remove-highlighter buffer/column
    }
} defer kaktree %{
    set-option global kaktree_indentation 1
    set-option global kaktree_show_help false
    # set-option global kaktree_dir_icon_open  '▾ 🗁 '
    # set-option global kaktree_dir_icon_close '▸ 🗀 '
    # set-option global kaktree_file_icon      '⠀⠀🖺'
}
