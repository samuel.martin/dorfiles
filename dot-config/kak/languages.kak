# general
# set spellcheck language to english
set-option global spell_lang en-GB

eval %sh{kak-lsp --kakoune -s $kak_session}

# check spelling before each saving
# hook global BufWritePre .* %{
#     spell
# }


# latex
hook global WinSetOption filetype=latex %{
    set buffer autowrap_column 80
    autowrap-enable
}

# Python
hook global WinSetOption filetype=python %{
    set buffer autowrap_column 80
    set-option buffer formatcmd 'autopep8'
    set-option buffer lintcmd "flake8 --filename='*' \
    --format='%%(path)s:%%(row)d:%%(col)d: error: %%(text)s' \
    --ignore=E121,E123,E126,E226,E24,E704,W503,W504,E501,E221,E127,E128,E129,F405"
    autowrap-enable
    expandtab
    lint-enable
    jedi-enable-autocomplete

    lint

	# add hook to automatically start lint as soon as we save the
	# buffer 
    hook buffer BufWritePre .* %{
        lint
    }
}

# C
hook global WinSetOption filetype=c %{

}

# C++
hook global WinSetOption filetype=cpp %{
    set-option window lintcmd "cppcheck --language=c++ \
    --enable=warning,style,information \
    --template='{file}:{line}:{column}: {severity}: {message}' \
    --suppress='*:*.h' --suppress='*:*.hh' 2>&1"
    clang-enable-autocomplete
    clang-enable-diagnostics

    # start lint and cland once when the file loads
    lint
    clang-parse

}

# C and C++
hook global WinSetOption filetype=(cpp|c) %{
    set-option buffer formatcmd 'clang-format'
    set-option buffer matching_pairs '{' '}' '[' ']' '(' ')'
	# add hook to automatically start lint and clang as soon as we save the
	# buffer 
    hook buffer BufWritePre .* %{
        lint
        clang-parse
    }

    # enable lsp
    lsp-enable-window
}

# json
hook global WinSetOption filetype=json %{
    set-option buffer formatcmd "jq --indent %opt{tabstob}"
}

# shell
hook global WinSetOption filetype=sh %{
    set-option window lintcmd 'shellcheck'
}
