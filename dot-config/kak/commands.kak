
define-command ide -override -docstring 'start IDE layout' %{
    rename-client main
    set global jumpclient main

	kaktree-enable
	set-option global kaktreeclient tree
	kaktree-toggle

	# move kaktree window to right side and size
	evaluate-commands %sh{
    	sleep 2;
		i3-msg -q move container left;
		i3-msg -q resize set width 300 px;
	}
}

define-command rofi-buffers \
-docstring 'Select an open buffer using Rofi' %{ evaluate-commands %sh{
    BUFFER=$(
    	printf "%s\n" "${kak_buflist}" |
    	tr " " "\n" |
    	rofi -dmenu -matching fuzzy -i| 
    	tr -d \')

    if [ -n "$BUFFER" ]; then
        printf "%s\n" "buffer ${BUFFER}"
    fi
	}
}
