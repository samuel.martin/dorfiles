#!/bin/bash
# Listens to hyprland events to trigger commands

handle() {
    case $1 in
        monitoradded*)
            kanshi reload;;
        monitorremoved*)
            kanshi reload;;
    esac
}

socat -U - UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock | while read -r line; do handle "$line"; done
