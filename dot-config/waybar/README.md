# My waybar setup

## Custom settings
The file `custom_settings.json` controls settings flags for custom waybar modules. This allows for toggeling custom
modules on or of without needing to edit the config file and restart waybar. This can be done by using `"exec-if":  "sh ~/.config/waybar/scripts/check_custom_settings.sh foo"` if the according entry in `custom_settings.json` is called `foo` which stores a boolean value.
