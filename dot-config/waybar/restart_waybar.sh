#!/bin/bash
# Restarts waybar, if an argument is given, uses the it as the path to the config file to use

if [[ $# -gt 0 ]]; then
    waybar_cmd="waybar --config $1"
else
    waybar_cmd="waybar"
fi

if [[ $(pgrep waybar | wc -l) -gt 0 ]]; then
    killall waybar && $waybar_cmd & disown
else
    $waybar_cmd & disown
fi

