#!/bin/bash
# Toggle mako mode from default to do-not-disturb and back
mode=$(makoctl mode)
case $mode in
    "default")
        notify-send "do not disturb on" "Enabled do not disturb mode"
        makoctl mode -s do-not-disturb
        /bin/kill -SIGRTMIN+1 $(pgrep waybar)
        ;;
    "do-not-disturb")
        makoctl mode -s default
        notify-send "do not disturb off" "Disabled do not disturb mode"
        /bin/kill -SIGRTMIN+1 $(pgrep waybar)
        ;;
    *)
        makoctl mode -s default
        /bin/kill -SIGRTMIN+1 $(pgrep waybar)
        ;;
esac
    
