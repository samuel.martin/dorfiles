#!/bin/bash
#
# Expects one arguments, exits with code 0 if the given argument is present and true in the custom setting files

CUSTOM_SETTINGS_FILE=~/.config/waybar/custom_settings.json

if [[ -z $1 ]]; then
    exit 1
fi

if [[ $(jq .$1 $CUSTOM_SETTINGS_FILE) == true ]]; then
    exit 0
else
    exit 1
fi
