proc_count="$(ps -ef | grep 'polybar touchbar' | wc -l)"
echo $proc_count
if [ $proc_count -gt 1 ]
then   
    /home/samuel/.config/polybar/kill_touch.sh
    echo "kill"
else
    /home/samuel/.config/polybar/start_touch.sh
    echo "start"
fi
