#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# check if laptop monitor is activated and launch bar if active
has_laptop=$( xrandr --listactivemonitors | awk '/(eDP-1|default)/ {print "yes"}' )
echo "Has Laptop $has_laptop"
if [[ $has_laptop == "yes" ]]
then
    monitor=$(xrandr --listactivemonitors | grep -o -E "(eDP-1|default)" | tail -n 1)
    echo "Launched bar for laptop monitor on $monitor"
    # PRIMARY_MONITOR=$monitor polybar --config=$1 mybar &
    PRIMARY_MONITOR=$monitor polybar --config=$1 primary &
fi


# get connected external monitor
ext_monitor=$( xrandr | awk '/^DP-[0-9,-]* connected/ {print $1}' )
echo "External monitor: $ext_monitor"
if [[ $ext_monitor = "" ]]
then
    echo "no external monitor"
else
    echo "launched bar for external monitor at $ext_monitor"
    MONITOR=$ext_monitor polybar --config=$1 secondScreen &
fi

