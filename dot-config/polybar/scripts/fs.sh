#!/bin/bash
function get_usage_info() {
    echo $(df -h $1 | awk '/dev/ {print $4 "/" $2}')
}
home=$( get_usage_info / )
data=$( get_usage_info /mnt/data )

echo " $home  $data"

