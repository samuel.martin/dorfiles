#!/bin/sh
output=''

# get string if bluetooth if powered
powered="$(bluetoothctl show | grep Powered)"
# only get yes or no
powered=${powered#*:}

# if powered get amount of connected devices
if [[ $powered -eq "yes" ]]
then
    # output bluetooth sign if bluetooth is enabled
    # output amount of paired devices
    paired_dev_count=$(bluetoothctl paired-devices | wc -l)
    if [[ $paired_dev_count -ge 1 ]]
    then
        output="$output 󰂱${paired_dev_count}"
    else
        output="$output 󰂯${paired_dev_count}"
    fi
else
    output=$"$output 󰂲"
fi

echo $output
