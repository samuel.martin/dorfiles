#!/bin/bash

# Function which takes the desktop name (0-9) as only parameter and returns the icon used for it if the desktop is
# active
function get_desktop_icon_active {
    case $1 in
        0)
            echo "";;
        1)
            echo "";;
        2)
            echo "";;
        3)
            echo "";;
        4)
            echo "";;
        5)
            echo "";;
        6)
            echo "";;
        7)
            echo "";;
        8)
            echo "";;
        9)
            echo "";;
    esac
}

# Function which takes the desktop name (0-9) as only parameter and returns the icon used for it if the desktop is
# deactive
function get_desktop_icon_deactive {
    case $1 in
        0)
            echo "";;
        1)
            echo "";;
        2)
            echo "";;
        3)
            echo "";;
        4)
            echo "";;
        5)
            echo "";;
        6)
            echo "";;
        7)
            echo "";;
        8)
            echo "";;
        9)
            echo "";;
    esac
}

function print_desktops {
    # Prints the desktops/worskpaces of bspwm, highlights the currently focused one
    desktops=($(bspc query -D -m $MONITOR --names | sort | tr '\n' ' '))
    current=$(bspc query -D -d --names)
    for i in "${!desktops[@]}"; do
        desktop=${desktops[$i]}
        if [[ $desktop = $current ]]; then
            icon=$(get_desktop_icon_active $desktop)
            desktops[i]="$icon"
        else
            icon=$(get_desktop_icon_deactive $desktop)
            desktops[i]="%{A1:bspc desktop -f $desktop:}$icon%{A}"
        fi
    done

    echo "${desktops[@]} "
}

print_desktops
bspc subscribe desktop_focus desktop_add  desktop_transfer desktop_remove | while read -r line; do
    print_desktops
done
