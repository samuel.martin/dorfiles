vpn_name=$(nmcli connection show --active | grep vpn | cut -d ' ' -f1)
if [[ $vpn_name == '' ]]; then
    echo ''
else
    
    echo '' $(echo $vpn_name | tr ' ' ':')
fi
